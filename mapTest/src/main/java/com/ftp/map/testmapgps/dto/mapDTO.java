package com.ftp.map.testmapgps.dto;

public class mapDTO {

	String latitude;
	String longitude;
	
	public mapDTO() {
		// TODO Auto-generated constructor stub
	}

	public mapDTO(String latitude, String longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	
}
